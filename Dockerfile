# # ---------getting base image ubuntu

# FROM ubuntu  
# RUN apt-get update -y && \
#     apt-get instaall -y python-pip python-dev
# # run command executed while building of image

# COPY ./requirements.txt /app/requirements.txt  

# # run command----> pip freeze > requirements.txt , it create requrements.txt file

# WORKDIR /app

# RUN pip install -r requirements.txt

# COPY . /app

# ENTRYPOINT [ "python" ]

# CMD [ "app.py" ]
# # CMD runs when we run containers out of image

#------------------alpine bassed image


from alpine:latest

RUN apk add py3-pip --no-cache python3-dev \
    && pip3 install --upgrade pip

WORKDIR /app

COPY . /app

RUN pip3 --no-cache-dir install -r requirements.txt                                                                            

EXPOSE 5000

ENTRYPOINT  ["python3"]

CMD ["app.py"]

# run <docker build -t flaskapp:latest .> this command to create docker image 
# docker login --username=mohit1talmale  password:Bunty@171
# docker tag flaskapp mohit1talmale/flaskapp:latest
# docker image push mohit1talmale/flaskapp:latest
 